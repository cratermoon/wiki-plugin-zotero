// build time tests for zotero plugin
// see http://mochajs.org/

;(function () {
  const zotero = require('../client/zotero'),
    expect = require('expect.js')

  describe('zotero plugin', () => {
    describe('expand', () => {
      it('can recognize a citation key', () => {
        var results = zotero.expand('[@helloWorld]')
        result = results.next()
        return expect(result.value[1]).to.be('helloWorld')
      })
      it('can handle multiple citation keys', () => {
        var results = zotero.expand('[@hello] and [@world], [@goodbye]')
        result = results.next()
        expect(result.value[1]).to.be('hello')
        result = results.next()
        expect(result.value[1]).to.be('world')
        result = results.next()
        expect(result.value[1]).to.be('goodbye')
        result = results.next()
        return expect(result.done).to.be(true)
      })
    })
    describe('authorCite', () => {
      it('formats author array', () => {
        var result = zotero.authorCite({author:[{ family: 'Newton' }]})
        return expect(result).to.be('Newton')
      })
    })
  })
}.call(this))
