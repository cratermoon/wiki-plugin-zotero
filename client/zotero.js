;(function () {
  async function cite (key) {
    const resp = await fetch('/plugin/zotero/' + key)
    const jsonBody = resp.json()
    const ref = await jsonBody
    return ref.ref
  }

  function expand (text) {
    return text.matchAll(/\[@(.+?)]/g)
  }

  function authorCite (ref) {
    authors = ref.author
    if (authors == undefined) {
      return ref.title
    }
    if (authors.length == 0) {
      return ref.title
    }
    if (authors.length == 1) {
      return authors[0].family
    }
    if (authors.length == 2) {
      return authors[0].family + ' and ' + authors[1].family
    }
    return authors[0].family + ' et al.'
  }

  async function emit ($item, item) {
    var url = "/plugins/zotero/zotero.css"
    if (!$('link[href="' + url + '"]').length)
        $('head').append('<link rel="stylesheet" type="text/css" href="' + url + '">');
    citeKeys = expand(item.text)
    var p = $('<p/>').addClass("zotero").appendTo($item)
    var ul = $('<ul/>').addClass("zotero").appendTo(p)
    $item.append(p)
    for (const key of citeKeys) {
      let ref = await cite(key[1])
      console.log('ref: ', ref)
      let authors = authorCite(ref)
      let pubDate = ref.issued['date-parts'][0][0]
      var li = $('<li/>')
        .addClass("zotero")
        .appendTo(ul)
      if (ref.URL == undefined || ref.URL == "") {
        li.text(`${ref.title} (${authors} ${pubDate})`)
      } else {
        li.html(`<a href="${ref.URL}" id="${ref.id}">${ref.title}</a> (${authors} ${pubDate})`)
      }
    }
    return $item
  }

  function bind ($item, item) {
    return $item.dblclick(() => {
      return wiki.textEditor($item, item)
    })
  }

  if (typeof window !== 'undefined' && window !== null) {
    window.plugins.zotero = { emit, bind }
  }

  if (typeof module !== 'undefined' && module !== null) {
    module.exports = { expand, authorCite, cite }
  }
}.call(this))
