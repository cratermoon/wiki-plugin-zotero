// zotero plugin, server-side component
// These handlers are launched with the wiki server.

var jsonfile = require('jsonfile')
var path = require('path')
;(function () {
  function startServer (params) {
    var app = params.app,
      argv = params.argv

    cors = function cors (req, res, next) {
      res.header('Access-Control-Allow-Origin', '*')
      return next()
    }
    return app.get('/plugin/zotero/:citekey', cors, (req, res) => {
      var citekey = req.params.citekey
      jsonfile.readFile(
        path.join(argv.data, 'library.json'),
        function (err, library) {
          if (err) {
            console.error(err)
            res.sendStatus(500)
            return
          }
          library.forEach(ref => {
            if (ref.id === citekey) {
              return res.json({ ref })
            }
          })
        }
      )
    })
  }

  module.exports = { startServer }
}.call(this))
