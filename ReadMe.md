# Federated Wiki - Zotero Plugin

This plugin, type: zotero, extends the markup of the federated wiki.

## Build

    npm install
    grunt build

## Configuration

There should be a
[Better CSL JSON](https://retorque.re/zotero-better-bibtex/installation/bundled-translators/#export)-formatted
file named `library.json` in
the root directory of the server.

A default library.json is included with this distribution.

## License

MIT
